import * as api from "../api";

const SET_OBJECT_ITEMS = 'GET_OBJECT_LIST',
    SET_CURRENT_OBJECT = 'SET_CURRENT_OBJECT';

const initialState = {
    objects: [],
    object_item: {}
};

export const getObjects = () =>
    (dispatch) => {
        api.fetchGetItems()
            .then((response) => response.json())
            .then((responseJson) => {
                dispatch({
                    type: SET_OBJECT_ITEMS,
                    objects:responseJson
                });
            })
    };


export const setCurrentObject = object_item =>
    (dispatch) => {
    console.log(object_item,'from objects')
        dispatch({
            type: SET_CURRENT_OBJECT,
            object_item
        });
    };


const ACTION_HANDLERS = {
    [SET_OBJECT_ITEMS]: (state, action) => ({
        ...state,
        objects: action.objects,
    }),
    [SET_CURRENT_OBJECT]: (state, action) => ({
        ...state,
        object_item: action.object_item,
    }),

};


export default function objects(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type];
    return handler ? handler(state, action) : state;
}

