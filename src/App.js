import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './styles/app.scss';
import Header from "./components/Header";
import Home from "./routes/AugmentedExperiencesList";
import AssetsList from "./routes/AssetsList";
import ObjectView from "./routes/ObjectView";
import TrainingImages from "./routes/TrainingImages.js";
import {Provider} from "react-redux";
import configureStore from './store/configureStore';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {persistStore} from 'redux-persist';

const store = configureStore();
const persistor = persistStore(store);
function App() {
  return (
      <Provider store={store}>
          {/*<PersistGate persistor={persistor}>*/}
              <Router>
                  <div className="App">
                      <div className="container-fluid">
                          <Header/>
                          <main>
                              <Route exact path="/" component={Home} />
                              <Route exact path="/assets-list" component={AssetsList} />
                              <Route exact path="/object-view" component={ObjectView} />
                              <Route exact path="/training-images" component={TrainingImages} />
                          </main>
                      </div>
                  </div>
              </Router>
          {/*</PersistGate>*/}
      </Provider>

  );
}

export default App;
