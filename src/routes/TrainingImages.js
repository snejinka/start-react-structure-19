import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ArrowIcon from '../images/next arrow.svg';

class TrainingImages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div>
                <div className="object-view">
                    <div className="subheader">
                        <div className="row">
                            <div className="col-md-4">
                                <button className="btn btn-back">
                                    <img src={ArrowIcon} className="subheader__arrow" alt=""/>
                                </button>
                                <span className="subheader__title">Add training images</span>
                            </div>
                            <div className="col-md-5">
                                <div className="row">
                                    <div className="col-md-6">
                                        <label className="custom-control switch justify-content-between">Select type of object:
                                            <input type="checkbox" className="switch-control-input"/>
                                            <span className="switch-control-indicator">
                                            <span className="switch-control-description switch-control-description_off">Inactive</span>
                                            <span className="switch-control-description switch-control-description_on">Active</span>
                                        </span>
                                        </label>
                                    </div>
                                    <div className="col-md-6 text-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => bindActionCreators({
},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TrainingImages);

