import React from 'react'
import { Link } from 'react-router-dom';
import ObjectImage from '../images/object-image1.png';
import bindIcon from "../images/bind-icon.svg";
import checkIcon from "../images/check-icon.svg";

const AssetCard = props => (
    <div className="asset__item checked">
        <img src={ObjectImage} alt="" className="asset__item-image"/>
        <div className="asset__item-content">
            <span className="asset__item-name">Asset name</span>
            <span className="asset__item-date">26.04.2019</span>
        </div>
        <span className="asset__item-format">
                <span className="asset__item-format-text">.jpg</span>
            </span>
        <span className="asset__item-status">
                <img className="asset__item-status-icon" src={bindIcon}/>
                Paired
            </span>
        <span className="asset__item-checkbox">
                <img className="asset__item-status-icon" src={checkIcon}/>
            </span>
    </div>
);
export default AssetCard;

